var express = require('express');
var router = express.Router();

var controllerIndex = require("../controllers/controllerIndex");
var controllerActivite = require("../controllers/controllerActivite");
var controllerD = require("../controllers/controllerD");
var controllerMedecin = require("../controllers/controllerMedecin");
var controllerDefault = require("../controllers/controllerDefault");

/* Connexion page. */
router.get('/', controllerDefault.choixProfile);
router.post('/loginSenior', controllerDefault.connexionSenior);
router.post('/loginMedecin', controllerDefault.connexionMedecin);

/*VerifConnection */
router.post('/loginSeniorV', controllerIndex.verificationConnexion);
router.post('/loginMedecinV', controllerMedecin.verificationConnexion);

/* controle Medecin */
router.get('/listeSeniors', controllerMedecin.getAllSenior);
router.get('/listeDemandes', controllerMedecin.getAllDemandes);

router.get('/indexMedecin', controllerMedecin.index);

/*controle globaux */
router.get('/logout', controllerIndex.deconnexion);

router.get('/index', controllerIndex.index);

router.post('/index', controllerIndex.index);

router.get('/profile', controllerIndex.getProfilUser);


/* ACTIVITES*/
router.get('/activites',controllerActivite.getAllActivites);



/*usr2*/
router.get('/MesSeances', controllerD.getAllSeances);
router.get('/MesSeances/:identifiant', controllerD.getAllSeancesActivite);
router.get('/AnnulInscription',controllerD.getAllSeancesAnnul);
router.post('/AnnulInscription',controllerD.annulInscription);


module.exports = router;
