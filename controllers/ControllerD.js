const pg = require('pg');

exports.getAllSeances=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://sheh:sheh@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

        console.log(req.session.user['numsenior']);
        const query = {
            name: 'fetch-all-seance',
            text: 'SELECT identifiant,designation,dateseance FROM activite a,seance s, participer p where a.identifiant = s.idact and p.codeseance = s.code and p.numsenior =$1;',
            values: [req.session.user['numsenior']]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('seancesDesActivites', {listeDesSeances: result.rows,user:req.session.user});
                    console.log(result.rows);
                }
                db.end();
            }
        );
    }
};
exports.getAllSeancesActivite=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://sheh:sheh@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

        //console.log(req.session.user['numsecu']);
        //console.log(req.params.value['designation']);
        const query = {
            name: 'fetch-all-seance',
            text: 'SELECT designation,dateseance FROM activite a,seance s, participer p where a.identifiant = s.idact and p.codeseance = s.code and p.numsenior =$1 and identifiant =$2;',
            values: [req.session.user['numsenior'],req.params.identifiant]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('seancesDesActivites', {listeDesSeances: result.rows,user:req.session.user});
                    console.log(result.rows);
                }
                db.end();
            }
        );
    }
};
exports.getAllSeancesAnnul=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://sheh:sheh@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

        console.log(req.session.user['numsenior']);
        const query = {
            name: 'fetch-all-seance',
            text: 'SELECT codeseance,identifiant,designation,dateseance FROM activite a,seance s, participer p where a.identifiant = s.idact and p.codeseance = s.code and p.numsenior =$1;',
            values: [req.session.user['numsenior']]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('annulinscription', {listeDesSeances: result.rows,user:req.session.user});
                    console.log(result.rows);
                }
                db.end();
            }
        );
    }
};
exports.annulInscription=function(req, res, next) {
    console.log(req.body.inscription);
    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://sheh:sheh@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

        console.log(req.session.user['numsenior']);
        const query = {
            name: 'supp-seance',
            text: 'delete from participer where numsenior =$1 and codeseance =$2;',
            values: [req.session.user['numsenior'],req.body.inscription]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('seancesDesActivites', {listeDesSeances: result.rows,user:req.session.user});
                    console.log(result.rows);
                }
                db.end();
            }
        );
    }
};