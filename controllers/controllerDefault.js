const pg = require('pg');

exports.choixProfile = function(req, res, next) {
    res.render('connexion', { title: 'ChoixProfil'});
};

exports.connexionSenior = function(req, res, next) {
    res.render('connectSenior', { title: 'Authentification'});
};

exports.connexionMedecin = function(req, res, next) {
    res.render('connectMedecin', { title: 'Authentification'});
};